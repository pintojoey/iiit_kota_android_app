package in.forsk.iiitk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class K_2013KUCP1029_SendMail extends Activity {

	EditText getemail;
	Button sendingmail;
	String messageToPost;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_k2013kucp1029_send_mail);
		getemail = (EditText) findViewById(R.id.getemail);
		sendingmail = (Button) findViewById(R.id.sendingmail);
		Bundle extras = getIntent().getExtras();
		messageToPost = extras.getString("app_link");

		sendingmail.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String mail = getemail.getText().toString();
				if (mail.isEmpty()) {
					Toast.makeText(getApplicationContext(), "Enter email",
							Toast.LENGTH_LONG);
				} else {
					Intent i = new Intent(Intent.ACTION_SEND);
					i.setType("message/rfc822");
					i.putExtra(Intent.EXTRA_EMAIL,
							new String[] { mail });
					i.putExtra(Intent.EXTRA_SUBJECT, "IIITK APP");
					i.putExtra(Intent.EXTRA_TEXT, ""+messageToPost);
					try {
						startActivity(Intent.createChooser(i, "Send mail..."));
						/*Intent j = new Intent(SendMail.this,ShareApp.class);
						startActivity(j);
						finish();*/
					} catch (android.content.ActivityNotFoundException ex) {
						Toast.makeText(getApplicationContext(),
								"There are no email clients installed.",
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		});

	}

}
