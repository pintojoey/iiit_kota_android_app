package in.forsk.iiitk.wrapper;

import org.json.JSONObject;

//In general, a wrapper class is any class which "wraps" or "encapsulates" the functionality of another class
//or component. These are useful by providing a level of abstraction from the 
//implementation of the underlying class or component

public class K_2013kucp1004_AcademicWrapper {
	
	private String month = "";
	private String date = "";
	private String title = "";
	
	
	public K_2013kucp1004_AcademicWrapper () {
		// TODO Auto-generated constructor stub
	}

	public K_2013kucp1004_AcademicWrapper (JSONObject jObj) {
		try {

			System.out.println(jObj.toString());

			month= jObj.getString("month");
			date = jObj.getString("date");
			title = jObj.getString("title");
		

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// define get and set method 
	
	
	public String getmonth() {
		return month;
	}

	public void setmonth(String month) {
		this.month = month;
	}

	public String getdate() {
		return date;
	}

	public void setdate(String date) {
		this.date = date;
	}

	public String gettitle() {
		return title;
	}

	public void settitle(String title) {
		this.title = title;
	}

	


}
