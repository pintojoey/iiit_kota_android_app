package in.forsk.iiitk.wrapper;

/**
 * Created by Timus on 2/11/2015.
 */


        import android.graphics.Bitmap;

        import org.json.JSONObject;

//In general, a wrapper class is any class which "wraps" or "encapsulates" the functionality of another class
//or component. These are useful by providing a level of abstraction from the
//implementation of the underlying class or component
//http://www.javawithus.com/tutorial/wrapper-classes
public class profileWrapper {

    private String header="";
    private String photo="";
    private String first_name = "";
    private String middle_name = "";
    private String last_name = "";
    private String collegeid = "";
    private String department = "";
    private String date_of_birth = "";
    private String batch = "";
    private String current_address = "";
    private String hometown="";
    private String city = "";
    private String state="";
    private String country="";
    private String email = "";
    private String phone="";
    private Bitmap pic;
    public profileWrapper() {
        // TODO Auto-generated constructor stub
    }

    public profileWrapper(JSONObject jObj) {
        try {

            System.out.println(jObj.toString());

            header = jObj.getString("FirstName")+jObj.getString("MiddleName")+jObj.getString("LastName");
            photo = jObj.getString("photo");
            first_name = jObj.getString("FirstName");
            middle_name = jObj.getString("MiddleName");
            last_name = jObj.getString("LastName");
            collegeid = jObj.getString("College ID");
            department = jObj.getString("Department");
            date_of_birth = jObj.getString("DateOfBirth");
            batch = jObj.getString("Batch");
            current_address = jObj.getString("CurrentAddress");
            hometown = jObj.getString("Hometown");
            state = jObj.getString("State");
            country = jObj.getString("Country");
            email = jObj.getString("Email");
            phone  = jObj.getString("Mobile");

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCollegeid() {
        return collegeid;
    }

    public void setCollegeid(String collegeid) {
        this.collegeid = collegeid;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getCurrent_address() {
        return current_address;
    }

    public void setCurrent_address(String current_address) {
        this.current_address = current_address;
    }

    public String getHometown() {
        return hometown;
    }

    public void setHometown(String hometown) {
        this.hometown = hometown;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Bitmap getPic() {
        return pic;
    }

    public void setPic(Bitmap pic) {
        this.pic = pic;
    }
}
