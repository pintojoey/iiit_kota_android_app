package in.forsk.iiitk.wrapper;

import java.util.ArrayList;

import org.json.JSONObject;

//In general, a wrapper class is any class which "wraps" or "encapsulates" the functionality of another class
//or component. These are useful by providing a level of abstraction from the 
//implementation of the underlying class or component
//http://www.javawithus.com/tutorial/wrapper-classes
public class Anita_2013kucp1020NewsListWrapper {

	private String date = "";
	private String title = "";
	private String subtitle = "";
	private String id= "";
	

	public Anita_2013kucp1020NewsListWrapper() {
		// TODO Auto-generated constructor stub
	}

	public Anita_2013kucp1020NewsListWrapper(JSONObject jObj) {
		try {

			System.out.println(jObj.toString());

			setDate(jObj.getString("Date"));
			setTitle(jObj.getString("Title"));
			setSubtitle(jObj.getString("Subtitle"));
			setId(jObj.getString("id"));
			

			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	

}
