package in.forsk.iiitk;


import in.forsk.iiitk.R;
import in.forsk.iiitk.adapter.k_2013kucp1009_SingleVideoAdapter;
import in.forsk.iiitk.wrapper.k_2013kucp1009_VideoListWrapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class k_2013kucp1009_VideoList extends Activity {
	
	        /* this class for  list of a given categories 
	            it  will open on click on categories of videoes */

	private final static String TAG = k_2013kucp1009_VideoGallery.class.getSimpleName();
	private Context context;
	String response = "";
    static int id;



    ArrayList<k_2013kucp1009_VideoListWrapper> mVideoDataList;
	ListView mVideoList;
	k_2013kucp1009_SingleVideoAdapter mAdapter;
	private TextView tv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_k_2013kucp1009_video_list);

		

		
		
		ListView samvideo=(ListView)findViewById(R.id.listView);
		
		tv = (TextView) findViewById(R.id.toolbarHeader);
		
		Bundle bundle = getIntent().getExtras();
		String str = bundle.getString("title");
		
		tv.setText(str);
		
		
		
		
		ImageView bk=(ImageView)findViewById(R.id.back);
		
		bk.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getApplicationContext() , k_2013kucp1009_VideoGallery.class);
				startActivity(intent);
				 finish();
			}
		});
		
  
		
		samvideo.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
			
				
				Intent intent = new Intent(getApplicationContext() , k_2013kucp1009_Video.class);
			
			    intent.putExtra("video_url", mVideoDataList.get(position).getvideo_url());
			
				intent.putExtra("video_title", mVideoDataList.get(position).getvideo_title());
				
				intent.putExtra("duration", mVideoDataList.get(position).getduration());
		
				intent.putExtra("description", mVideoDataList.get(position).getdescription());
				
				startActivity(intent);
				
			}});
		
		context = this;
		mVideoList = (ListView) findViewById(R.id.listView);
		
	
			new CustomAsyncTask().execute();
			

	}

	private String openHttpConnection(String urlStr) throws IOException {
		InputStream in = null;
		int resCode = -1;

		try {
			URL url = new URL(urlStr);
			URLConnection urlConn = url.openConnection();

			if (!(urlConn instanceof HttpURLConnection)) {
				throw new IOException("URL is not an Http URL");
			}

			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			resCode = httpConn.getResponseCode();
			if (resCode == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return convertStreamToString(in);
	}

	private String convertStreamToString(InputStream is) throws IOException {
		// Converting input stream into string
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = is.read();
		while (i != -1) {
			baos.write(i);
			i = is.read();
		}
		return baos.toString();
	}

	// Params, Progress, Result
	class CustomAsyncTask extends AsyncTask<Void, Void, String> {

		String url;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			
			url = "http://online.mnit.ac.in/iiitk/assets/video_gallery.php?id="+id;
			
			Toast.makeText(context, "onPreExecute", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected String doInBackground(Void... params) {
			String response = "";
			try {
				response = openHttpConnection(url);
				
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println(response);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

		   mVideoDataList = pasreLocalVideoFile(result);
			setVideoListAdapter(mVideoDataList);
			
			Toast.makeText(context, "onPostExecute", Toast.LENGTH_SHORT).show();
		}
		
		
	}
	
	
	
	
	
	
	
	

	public void setVideoListAdapter(ArrayList<k_2013kucp1009_VideoListWrapper> mVideoDataList) {
		mAdapter = new k_2013kucp1009_SingleVideoAdapter(context, mVideoDataList);
		mVideoList.setAdapter(mAdapter);
	}

	public ArrayList<k_2013kucp1009_VideoListWrapper> pasreLocalVideoFile(String json_string) {

		ArrayList<k_2013kucp1009_VideoListWrapper> mVideoList = new ArrayList<k_2013kucp1009_VideoListWrapper>();
		try {
			// Converting multipal json data (String) into Json array
			JSONArray videoArray = new JSONArray(json_string);
			Log.d(TAG, videoArray.toString());
			// Iterating json array into json objects
			for (int i = 0; videoArray.length() > i; i++) {

				// Extracting json object from particular index of array
				JSONObject videoJsonObject = videoArray.getJSONObject(i);

				// Design patterns
				k_2013kucp1009_VideoListWrapper videoObject = new k_2013kucp1009_VideoListWrapper(videoJsonObject);

				printObject(videoObject);

				mVideoList.add(videoObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mVideoList;
	}

	public void printObject(k_2013kucp1009_VideoListWrapper obj) {
		// Operator Overloading
		Log.d(TAG, "Link of video : " + obj.getvideo_url());
		Log.d(TAG, "thumbnail: " + obj.getthumbnail());
		
		Log.d(TAG, "Title of video : " + obj.getvideo_title());
		Log.d(TAG, "Description of video : " + obj.getdescription());
		Log.d(TAG, "Duration of video : " + obj.getduration());
		
		
		
	}

	
	
	
	
}
