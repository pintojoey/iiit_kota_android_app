//package in.forsk.iiitk;
//
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.HttpURLConnection;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.net.URLConnection;
//import java.util.ArrayList;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.ListView;
//import android.widget.Toast;
//import in.forsk.iiitk.wrapper.K2013kucp1003_Faculty_Wrapper;
//
//public class K2013kucp1003_Click extends Activity {
//
//	private static final String TAG = "Priya" ;
//    Context context;
//    ArrayList<K2013kucp1003_Faculty_Wrapper> mFacultyDataList;
//    String response="";
//    K2013kucp1003_Faculty_list_adapter mAdapter;
//    ListView FacultyList;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_k2013kucp1003__faculty_profile_main);
//        context=this;
//        new CustomAsyncTask().execute();
//        FacultyList=(ListView)findViewById(R.id.facultyList);
//        FacultyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                K2013kucp1003_Faculty_detail.name=mFacultyDataList.get(position).getName();
//                K2013kucp1003_Faculty_detail.department=mFacultyDataList.get(position).getDepartment();
//                K2013kucp1003_Faculty_detail.hometown=mFacultyDataList.get(position).getCity();
//                K2013kucp1003_Faculty_detail.email=mFacultyDataList.get(position).getEmail();
//                K2013kucp1003_Faculty_detail.phone=mFacultyDataList.get(position).getPhone();
//                K2013kucp1003_Faculty_detail.photo=mFacultyDataList.get(position).getPhoto();
//                K2013kucp1003_Faculty_detail.photo=mFacultyDataList.get(position).getQualifications();
//                K2013kucp1003_Faculty_detail.photo=mFacultyDataList.get(position).getFacebook();
//                K2013kucp1003_Faculty_detail.photo=mFacultyDataList.get(position).getAchievements();
//                K2013kucp1003_Faculty_detail.photo=mFacultyDataList.get(position).getReserch_area();
//                
//                Intent next = new Intent(getBaseContext(),K2013kucp1003_Faculty_detail.class);
//                startActivity(next);
//
//            }
//        });
//    }
//
//
//    /*@Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_student_list, menu);
//        return true;
//    }
//0
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//*/
//    private String openHttpConnection(String urlStr) throws IOException {
//        InputStream in = null;
//        int resCode = -1;
//
//        try {
//            URL url = new URL("Your Link");
//            URLConnection urlConn = url.openConnection();
//
//            if (!(urlConn instanceof HttpURLConnection)) {
//                throw new IOException("Page Not Found");
//            }
//
//            HttpURLConnection httpConn = (HttpURLConnection) urlConn;
//            httpConn.setAllowUserInteraction(false);
//            httpConn.setInstanceFollowRedirects(true);
//            httpConn.setRequestMethod("GET");
//            httpConn.connect();
//
//            resCode = httpConn.getResponseCode();
//            if (resCode == HttpURLConnection.HTTP_OK) {
//                in = httpConn.getInputStream();
//                System.out.println("x"+in);
//            }
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return convertStreamToString(in);
//    }
//
//    private String convertStreamToString(InputStream is) throws IOException {
//        // Converting input stream into string
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        int i = is.read();
//
//        while (i != -1) {
//            baos.write(i);
//            i = is.read();
//
//        }
//        return baos.toString();
//    }
//
//    // Params, Progress, Result
//    class CustomAsyncTask extends AsyncTask<Void, Void, String> {
//
//        String url;
//        String result = "";
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//
//            Toast.makeText(context, "Student List is Loading...", Toast.LENGTH_SHORT).show();
//        }
//
//        @Override
//        protected String doInBackground(Void... params) {
//
//            try {
//                result = openHttpConnection(url);
//
//                try {
//                    Thread.sleep(5000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            return result;
//        }
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//          response=result;
//            mFacultyDataList= parseFacultyFile(response);
//            setFacultyListAdapter(mFacultyDataList);
//            Toast.makeText(context, "Student List has been Loaded", Toast.LENGTH_SHORT).show();
//        }
//
//
//    }
//    public void setFacultyListAdapter(ArrayList<K2013kucp1003_Faculty_Wrapper> mStudentDataList) {
//        mAdapter = new K2013kucp1003_Faculty_list_adapter(context, mStudentDataList);
//        FacultyList.setAdapter(mAdapter);
//    }
//
//    public ArrayList<K2013kucp1003_Faculty_Wrapper> parseFacultyFile(String json_string) {
//
//        ArrayList<K2013kucp1003_Faculty_Wrapper> mFacultyDataList = new ArrayList<K2013kucp1003_Faculty_Wrapper>();
//        try {
//            // Converting multipal json data (String) into Json array
//            JSONArray FacultyArray = new JSONArray(json_string);
//            Log.d(TAG, FacultyArray.toString());
//            // Iterating json array into json objects
//            for (int i = 0; FacultyArray.length() > i; i++) {
//
//                // Extracting json object from particular index of array
//                JSONObject FacultyJsonObject = FacultyArray.getJSONObject(i);
//
//                // Design patterns
//               K2013kucp1003_Faculty_Wrapper FacultyObject = new K2013kucp1003_Faculty_Wrapper(FacultyJsonObject);
//
//
//
//                mFacultyDataList.add(FacultyObject);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return mFacultyDataList;
//    }
//
//}
