package in.forsk.iiitk;



import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class K_2013kucp1012_ResourcesListAdapter extends BaseAdapter{
	
	
	

//	private final static String TAG = AnouncementListAdapter.class.getSimpleName();
	Context context;
	ArrayList<K_2013kucp1012_ResourcesWrapper> mResourcesDataList;
	LayoutInflater inflater;
	ViewHoder holder;

	
	public K_2013kucp1012_ResourcesListAdapter(Context context, ArrayList<K_2013kucp1012_ResourcesWrapper> mResourcesDataList) {
		this.context = context;
		this.mResourcesDataList = mResourcesDataList;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.k_2013kucp1012_row_resources_list, null);

			holder = new ViewHoder(convertView);

			convertView.setTag(holder);
		} else {

			// this is called when you flick the list to see the other item.
			// (Or can say whem getView method reuse the view)
			holder = (ViewHoder) convertView.getTag();
		}

		K_2013kucp1012_ResourcesWrapper obj =mResourcesDataList.get(position);

		

		holder.name.setText(obj.getname());
		holder.date.setText(obj.getdate());
		
		holder.detail.setText(obj.getdetail());

		return convertView;
	}
	
	
	
	public static class ViewHoder {
		
		TextView date,  detail,name;

		public ViewHoder(View view) {
			

			detail = (TextView) view.findViewById(R.id.detail);
			date= (TextView) view.findViewById(R.id.date);
			//time= (TextView) view.findViewById(R.id.time);
			name= (TextView) view.findViewById(R.id.name);
			//general= (TextView) view.findViewById(R.id.general);
		}
	}
	
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mResourcesDataList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return  mResourcesDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}


	
	

}
