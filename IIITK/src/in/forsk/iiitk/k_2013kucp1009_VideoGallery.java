package in.forsk.iiitk;


import in.forsk.iiitk.R;
import in.forsk.iiitk.adapter.k_2013kucp1009_VideoListAdapter;
import in.forsk.iiitk.wrapper.k_2013kucp1009_VideoWrapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class k_2013kucp1009_VideoGallery extends Activity {       /*    it is main class for video gallery  it  will show categories of videos   */

	private final static String TAG = k_2013kucp1009_VideoGallery.class.getSimpleName();
	private Context context;
	String response = "";




	ArrayList<k_2013kucp1009_VideoWrapper> mFacultyDataList ;
	GridView mFacultyList;
	k_2013kucp1009_VideoListAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_k_2013kucp1009_video_gallery);
		
		Log.d(TAG, "virus");
		
		
		
ImageView bk=(ImageView)findViewById(R.id.back);
		
		bk.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getApplicationContext() , Dashboard.class);
				startActivity(intent);
				 finish();
			}
		});
		
		
		
		
		
		GridView videogrid=(GridView) findViewById(R.id.gridView);
		
		videogrid.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				k_2013kucp1009_VideoList.id=position;
				Intent intent = new Intent(getApplicationContext() , k_2013kucp1009_VideoList.class);
				intent.putExtra("title", mFacultyDataList.get(position).getTitle());
				startActivity(intent);
				
			}});
		
		
		context = this;

		mFacultyList = (GridView) findViewById(R.id.gridView);
	
			new CustomAsyncTask().execute();
			
			
			
			
			

	}

	private String openHttpConnection(String urlStr) throws IOException {
		InputStream in = null;
		int resCode = -1;

		try {
			URL url = new URL(urlStr);
			URLConnection urlConn = url.openConnection();

			if (!(urlConn instanceof HttpURLConnection)) {
				throw new IOException("URL is not an Http URL");
			}

			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.setAllowUserInteraction(false);
			httpConn.setInstanceFollowRedirects(true);
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			resCode = httpConn.getResponseCode();
			if (resCode == HttpURLConnection.HTTP_OK) {
				in = httpConn.getInputStream();
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return convertStreamToString(in);
	}

	private String convertStreamToString(InputStream is) throws IOException {
		// Converting input stream into string
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int i = is.read();
		while (i != -1) {
			baos.write(i);
			i = is.read();
		}
		return baos.toString();
	}

	// Params, Progress, Result
	class CustomAsyncTask extends AsyncTask<Void, Void, String> {

		String url;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			url = "http://online.mnit.ac.in/iiitk/assets/videogallery.json";
			        /*   it is url for json for categories video */
		
			Toast.makeText(context, "onPreExecute", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected String doInBackground(Void... params) {
			String response = "";
			try {
				response = openHttpConnection(url);
				
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println(response);
			return response;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			 mFacultyDataList = pasreLocalVideoFile(result);
			setVideoListAdapter(mFacultyDataList);
			
			Toast.makeText(context, "onPostExecute", Toast.LENGTH_SHORT).show();
		}
		
		
	}
	
	
	
	
	
	
	
	

	public void setVideoListAdapter(ArrayList<k_2013kucp1009_VideoWrapper> mFacultyDataList) {
		mAdapter = new k_2013kucp1009_VideoListAdapter(context, mFacultyDataList);
		mFacultyList.setAdapter(mAdapter);
	}

	public ArrayList<k_2013kucp1009_VideoWrapper> pasreLocalVideoFile(String json_string) {

		ArrayList<k_2013kucp1009_VideoWrapper> mFacultyDataList = new ArrayList<k_2013kucp1009_VideoWrapper>();
		try {
			// Converting multipal json data (String) into Json array
			JSONArray facultyArray = new JSONArray(json_string);
			Log.d(TAG, facultyArray.toString());
			// Iterating json array into json objects
			for (int i = 0; facultyArray.length() > i; i++) {

				// Extracting json object from particular index of array
				JSONObject facultyJsonObject = facultyArray.getJSONObject(i);

				// Design patterns
				k_2013kucp1009_VideoWrapper facultyObject = new k_2013kucp1009_VideoWrapper(facultyJsonObject);

				printObject(facultyObject);

				mFacultyDataList.add(facultyObject);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mFacultyDataList;
	}

	public void printObject(k_2013kucp1009_VideoWrapper obj) {
		// Operator Overloading                       /*  to print and check in logCat */
		Log.d(TAG, "Title : " + obj.getTitle());
		Log.d(TAG, "Link Image : " + obj.getImage_url());
		Log.d(TAG, "no. of video : " + obj.getVideo_no());
		
	}

	
	
	
	
}
