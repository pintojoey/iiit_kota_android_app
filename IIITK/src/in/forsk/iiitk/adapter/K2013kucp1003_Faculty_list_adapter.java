//package in.forsk.iiitk.adapter;
//
//import in.forsk.iiitk.R;
//import in.forsk.iiitk.wrapper.FacultyWrapper;
//
//import java.util.ArrayList;
//
//import com.androidquery.AQuery;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//public class FacultyListAdapter extends BaseAdapter {
//	private final static String TAG = FacultyListAdapter.class.getSimpleName();
//	Context context;
//	ArrayList<FacultyWrapper> mFacultyDataList;
//	LayoutInflater inflater;
//	ViewHoder holder;
//
//	AQuery aq;
//
//	public FacultyListAdapter(Context context, ArrayList<FacultyWrapper> mFacultyDataList) {
//		this.context = context;
//		this.mFacultyDataList = mFacultyDataList;
//
//		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//		aq = new AQuery(context);
//	}
//
//	@Override
//	public int getCount() {
//		return mFacultyDataList.size();
//	}
//
//	@Override
//	public Object getItem(int position) {
//		return mFacultyDataList.get(position);
//	}
//
//	@Override
//	public long getItemId(int position) {
//		return position;
//	}
//
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
//		if (convertView == null) {
//			convertView = inflater.inflate(R.layout.row_faculty_profile_list, null);
//
//			holder = new ViewHoder(convertView);
//
//			convertView.setTag(holder);
//		} else {
//
//			// this is called when you flick the list to see the other item.
//			// (Or can say whem getView method reuse the view)
//			holder = (ViewHoder) convertView.getTag();
//		}
//
//		FacultyWrapper obj = mFacultyDataList.get(position);
//
//		AQuery temp_aq = aq.recycle(convertView);
//		temp_aq.id(holder.profileIv).image(obj.getPhoto(), true, true, 200, 0);
//
//		holder.nameTv.setText(obj.getFirst_name() + " " + obj.getLast_name());
//		holder.departmentTv.setText(obj.getDepartment());
//		holder.reserch_areaTv.setText(obj.getReserch_area());
//
//		return convertView;
//	}
//
//	public static class ViewHoder {
//		ImageView profileIv;
//		TextView nameTv, departmentTv, reserch_areaTv;
//
//		public ViewHoder(View view) {
//			profileIv = (ImageView) view.findViewById(R.id.profileIv);
//
//			nameTv = (TextView) view.findViewById(R.id.nameTv);
//			departmentTv = (TextView) view.findViewById(R.id.departmentTv);
//			reserch_areaTv = (TextView) view.findViewById(R.id.reserch_areaTv);
//		}
//	}
//
//}
